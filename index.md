<html><head><meta charset='utf-8'><meta id="ForPhone" name="viewport" content="width=device-width, initial-scale=1.0"><title>Ma page 2nd bac pro</title> </head>

  

  
  
<body>
  
  
# Ressources 2nd bac pro

  <br>

<div> Dépôt contenant les ressources pour la classe de 2nd professionnelle.
  
  Pour retrouver les autres niveaux, [cliquez sur ce lien](https://tmounier.forge.aeif.fr/maths-sciences/).</div>


  
<p><strong>Les ressources  </strong></p>
  

<div> Les documents sont disponibles, la plupart du temps, au format .tex (LaTeX) ou au format .pdf pour une utilisation direct en cas de besoin.
</div>

---
  
## Maths
  
0. [Des activités autour de Python et de l'algorithmie](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Python%20algo)  
1. [Probabilités](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Probas)
2. [Géométrie](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Geometrie)
3. [Problèmes du premier degré](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Premier%20degré)
4. [Statistiques](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Stats) (à venir)
5. Fonctions 1
6. Fonctions 2
7. Vers la première

Et aussi des [activités diverses](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Maths/Maths%20divers).

## Physique - chimie


1. [Démarche scientifique et compte rendu de TP.](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Sciences/Démarche%20scientifique)
2. [Mécanique 1](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Sciences/Mécanique)
  
Cette partie restera peu remplie cette année, partageant ma classe de 2nd avec un collègue, c'est lui qui gère les sciences ! J'ai déposé ce que j'avais préparé avant ce partage. ;)  
## Le livret de mathématiques

Je mets à dispostion de mes élèves un [livret](https://forge.aeif.fr/tmounier/2nd-tex/-/blob/master/Maths/livret%20maths/livret%20seconde.pdf) contenant des exercices. 
  Ces exercices sont de l'application de cours, des prises d'initiative, des problèmes ouverts. 
  Ce document remplace (partiellement) un manuel et se veut un compagnon pour l'autonomie de l'élève, l'accompagnement personnalisé et l'approfondissement.
 
## Pour la classe
  
Dans ce [répertoire](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Divers) quelques activité inclassables (bilans, prof principal...)
  

## Des ressources .tex 
  
Afin d'ouvrir et de compiler les documents, j'ai aussi placé [à la racine du dépôt](https://forge.aeif.fr/tmounier/2nd-tex/-/tree/master/Packages) mes packages persos.


## Des ressources libres
  
  
En attente d'une plus profonde réflexion (et gestion des conflits), mes ressources sont distribuées sous licence 
  ![](https://forge.aeif.fr/tmounier/2nd-tex/-/raw/master/ccby.png)


  </body></html>
def verif(a):
    """ Pour utiliser taper verif('mot de passe') """
    taille = len(a)
    if taille <6:
        return("Le mot de passe est trop court")
    else:
        return("Le mot de passe a une longueur satisfaisante")
		
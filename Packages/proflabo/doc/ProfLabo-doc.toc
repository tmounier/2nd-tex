\babel@toc {french}{}\relax 
\babel@toc {french}{}\relax 
\contentsline {chapter}{\numberline {1}Pourquoi ce package}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Les commandes disponibles}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}Fonctionnement général avec clés}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Tube à essai}{5}{section.2.2}%
\contentsline {section}{\numberline {2.3}Porte-tubes avec n tubes}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}Bécher}{7}{section.2.4}%
\contentsline {section}{\numberline {2.5}Fiole jaugée}{8}{section.2.5}%
\contentsline {section}{\numberline {2.6}Erlenmeyer}{9}{section.2.6}%
\contentsline {section}{\numberline {2.7}Dosage}{10}{section.2.7}%
\contentsline {chapter}{\numberline {3}Exemples}{11}{chapter.3}%
\contentsline {chapter}{\numberline {4}Historique}{15}{chapter.4}%
